// SPDX-License-Identifier: Apache-2.0 OR MIT
// Copyright: Gabriel Marcano, 2022

use rocketlogger::*;
use std::fs::File;

fn main() -> Result<(), RLDError> {
    let mut file = File::open("test.rld")?;
    let mut rld = RLDFile::parse(&mut file)?;
    println!("{:#?}", rld.header);
    for _ in 0..10 {
        let block = rld.next_block()?;
        for i in 0..block.samples.len() {
            println!("{} {:#?}", i, block.samples[i]);
        }
    }
    let ctx = zmq::Context::new();
    let socket = ctx.socket(zmq::SUB).unwrap();
    let socket2 = ctx.socket(zmq::SUB).unwrap();
    socket.set_subscribe(b"").unwrap();
    socket2.set_subscribe(b"").unwrap();
    socket.connect("tcp://localhost:8080").unwrap();
    socket2.connect("tcp://localhost:8081").unwrap();
    let mut msg = zmq::Message::new();
    socket.recv(&mut msg, 0).unwrap();
    let metadata = parse_metadata(&msg).unwrap();
    println!("{:?}", metadata);
    let data = socket2.recv_multipart(0).unwrap();
    println!("{:?}", parse_data(&metadata, &data));

    Ok(())
}
