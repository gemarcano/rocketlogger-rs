// SPDX-License-Identifier: Apache-2.0 OR MIT
// Copyright: Gabriel Marcano, 2022

use byteorder::ByteOrder;
use byteorder::LittleEndian;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Debug)]
pub struct RLConfig {
    pub ambient_enable: bool,
    pub background_enable: bool,
    pub calibration_ignore: bool,
    pub channel_enable: Vec<String>,
    pub channel_force_range: Vec<bool>,
    pub digital_enable: bool,
    pub file: serde_json::Value, // this thing is Null if invalid
    pub interactive_enable: bool,
    pub sample_limit: u64,
    pub sample_rate: u32,
    pub update_rate: u32,
    pub web_enable: bool,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct RLFrameHeader {
    pub buffer_count: u64,
    pub calibration_file: String,
    pub calibration_time: u64,
    pub config: RLConfig,
    pub disk_free_bytes: u64,
    pub disk_free_permille: u16,
    pub disk_use_rate: u32,
    pub error: bool,
    pub sample_count: u64,
    pub sampling: bool,
    pub sensor_count: u16,
    pub sensor_names: Vec<serde_json::Value>, // These can also be null...
}

#[derive(Serialize, Deserialize, Debug)]
pub struct RLMetadata {
    pub channels: Vec<RLChannelHeader>,
    pub data_rate: u32,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(untagged)]
pub enum RLChannelHeader {
    Analog(RLAnalogChannelHeader),
    Binary(RLBinaryChannelHeader),
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct RLAnalogChannelHeader {
    pub name: String,
    pub scale: f64,
    pub unit: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct RLBinaryChannelHeader {
    pub bit: u8,
    pub hidden: bool,
    pub name: String,
    pub unit: String,
}

#[derive(Debug)]
pub struct RLAnalogChannel {
    pub name: String,
    pub unit: String,
    pub data: Vec<f64>,
}

#[derive(Debug)]
pub enum Error {
    JSON(serde_json::Error),
    Format,
    Conversion(std::num::TryFromIntError),
}

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub struct RLStreamData {
    pub metadata: RLMetadata,
    pub realtime_ts: chrono::NaiveDateTime,
    pub monotime_ts: chrono::NaiveDateTime,
    pub analog_channels: HashMap<String, RLAnalogChannel>,
    pub binary_channel: Vec<u32>,
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match *self {
            Error::JSON(ref err) => err.fmt(f),
            Error::Format => write!(f, "rocketlogger data is malformatted"),
            Error::Conversion(ref err) => err.fmt(f),
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match *self {
            Error::JSON(ref err) => Some(err),
            Error::Format => None,
            Error::Conversion(ref err) => Some(err),
        }
    }
}

impl From<serde_json::Error> for Error {
    fn from(err: serde_json::Error) -> Error {
        Error::JSON(err)
    }
}

impl From<std::num::TryFromIntError> for Error {
    fn from(err: std::num::TryFromIntError) -> Error {
        Error::Conversion(err)
    }
}

pub fn parse_metadata(raw_metadata: &[u8]) -> serde_json::Result<RLFrameHeader> {
    serde_json::from_slice(raw_metadata)
}

pub fn parse_data(metadata: &RLFrameHeader, raw_data: &[Vec<u8>]) -> Result<RLStreamData> {
    println!("{}", std::str::from_utf8(&raw_data[0]).unwrap());
    // First message contains the metadata
    let block_metadata: RLMetadata = serde_json::from_slice(&raw_data[0])?;

    // Second message contains timestamps
    let realtime_s = LittleEndian::read_i64(&raw_data[1][0..8]);
    let realtime_ns: u32 = LittleEndian::read_i64(&raw_data[1][8..16]).try_into()?;
    let realtime_ts = chrono::NaiveDateTime::from_timestamp(realtime_s, realtime_ns);

    let monotime_s = LittleEndian::read_i64(&raw_data[1][16..24]);
    let monotime_ns: u32 = LittleEndian::read_i64(&raw_data[1][24..32]).try_into()?;
    let monotime_ts = chrono::NaiveDateTime::from_timestamp(monotime_s, monotime_ns);

    // Next messages contain analog channel data. The number of messages equal the number of
    // enabled analog channels in the RLFrameHeader
    let mut analog_channels = HashMap::<String, RLAnalogChannel>::new();
    let data_rate = block_metadata.data_rate;
    let update_rate = metadata.config.update_rate;

    let data_per_sample = if data_rate < 1000 { 1000 } else { data_rate };
    let data_per_update = data_per_sample / update_rate;
    let channel_enable = metadata.config.channel_enable.len();
    for i in 0..channel_enable {
        let channel = &block_metadata.channels[i];
        let mut channel_data = Vec::<f64>::new();
        match channel {
            RLChannelHeader::Analog(analog) => {
                let data = &raw_data[2 + i];

                for j in 0..data_per_update {
                    let index: usize = (j * 4).try_into()?;
                    let value = LittleEndian::read_i32(&data[index..(index + 4)]);
                    channel_data.push(analog.scale * f64::from(value));
                }
                let value = RLAnalogChannel {
                    name: analog.name.clone(),
                    unit: analog.unit.clone(),
                    data: channel_data,
                };
                analog_channels.insert(analog.name.clone(), value);
            }
            RLChannelHeader::Binary(_binary) => {
                // Error out if we encounter a binary channel -- we shouldn't find any yet
                return Err(Error::Format);
            }
        }
    }

    // The next message has ambient data, if it's enabled, else it is skipped
    // FIXME Gabriel Marcano: this hasn't really been tested, as apparently my RL doesn't have or
    // can't enable ambient sensors
    let mut current_message = channel_enable;
    if metadata.config.ambient_enable {
        // If ambient data is enabled, each one is sent in a new message
        let data = &raw_data[2 + current_message];
        for _i in 0..metadata.sensor_count {
            // Each sensor is an update_rate's worth of i32 of data.
            let mut _ambient_data = Vec::<i32>::new();
            for j in 0..data_per_update {
                let index: usize = (j * 4).try_into()?;
                let value = LittleEndian::read_i32(&data[index..(index + 4)]);
                _ambient_data.push(value);
            }
            //FIXME save data read out
            current_message += 1;
        }
    }
    // just for kicks, make variable immutable again
    let current_message = current_message;

    // Lastly, the binary data is in the last message
    let mut binary_channel = Vec::<u32>::new();
    let binary_channel_metadata = &block_metadata.channels[current_message..];
    for channel in binary_channel_metadata {
        match channel {
            RLChannelHeader::Analog(_analog) => {
                return Err(Error::Format);
            }
            RLChannelHeader::Binary(_binary) => {
                let data = &raw_data[2 + current_message];
                for j in 0..data_per_update {
                    let index: usize = (j * 4).try_into()?;
                    // FIXME Not sure if this is a bug in RocketLogger, but the documentation indicates
                    // that there can be more than 32 bits of binary data, but the socket handler
                    // currently only ever sends over 32 bits...
                    let value = LittleEndian::read_u32(&data[index..(index + 4)]);
                    binary_channel.push(value);
                }
            }
        }
    }

    Ok(RLStreamData {
        metadata: block_metadata,
        realtime_ts,
        monotime_ts,
        analog_channels,
        binary_channel,
    })
}
