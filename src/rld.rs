// SPDX-License-Identifier: Apache-2.0 OR MIT
// Copyright: Gabriel Marcano, 2022

use byteorder::LittleEndian;
use byteorder::ReadBytesExt;
use chrono::NaiveDateTime;
use std::error;
use std::fmt;
use std::io;
use std::io::Read;
use std::num::TryFromIntError;
use std::string::FromUtf8Error;

pub struct RLDFile<'a> {
    pub header: RLDHeader,
    file: &'a mut dyn Read,
}

/// Header of an RLD file
#[derive(Debug)]
pub struct RLDHeader {
    /// Magic 4 bytes of RLD file, should be "%RLD"
    pub magic: [u8; 4],
    /// Current file version
    pub version: u16,
    /// Length of the header in bytes, including the comment
    pub length: u16,
    /// The number of samples in a block
    pub block_size: u32,
    /// The number of blocks (last block may not be full)
    pub block_count: u32,
    /// The total number of samples taken
    pub sample_count: u64,
    /// The sample rate (samples per second)
    pub sampling_rate: u16,
    /// The MAC address of the RocketLogger (stored in big endian)
    pub mac_address: [u8; 6],
    /// Starting timestamp in POSIX seconds
    pub start_time_s: i64,
    /// Fractional part of the starting timestamp, in nanoseconds
    pub start_time_ns: i64,
    /// Length of the comments (multiple of 4)
    pub comment_length: u32,
    /// The number of binary channels in the data
    pub binary_channel_count: u16,
    /// The number of analog channels in the data
    pub analog_channel_count: u16,
    /// The comment associated with the data, the length in the comment_length field. The size is a
    /// multiple of 4, and if the text is not long enough, it is padded with null characters
    pub comment: String,
    /// Data about each channel
    pub channels: Vec<RLDChannel>,
}

#[derive(Debug)]
pub enum RLDUnit {
    UnitLess,
    Voltage,
    Current,
    Binary,
    DataValid,
    Illuminance,
    Temperature,
    Integer,
    Percent,
    Pressure,
    Undefined,
}

/// Channel data
#[derive(Debug)]
pub struct RLDChannel {
    /// The unit of the channel
    pub unit: RLDUnit,
    /// The scaling that needs to be applied to the raw data. The scaling that needs to be applied
    /// is 10 to the scaling value
    pub scale: i32,
    /// The number of bytes per sample
    pub data_size: u16,
    /// Index in binary data representing whether the channel is valid or not
    pub valid_data_link: u16,
    /// Name of the channel
    pub name: String,
}

/// RLD Block
#[derive(Debug)]
pub struct RLDBlock {
    /// Timestamp using POSIX origin, with nanosecond precision
    pub realtime_ts: NaiveDateTime,
    /// Monotonic timestamp, with nanosecond precision
    pub monotonic_ts: NaiveDateTime,
    /// Samples in the block
    pub samples: Vec<RLDSample>,
}

#[derive(Debug)]
pub struct RLDSample {
    /// The binary channels/data in the sample, the number specified by the binary_channel_count in
    /// the header
    pub binary_values: Vec<u32>,
    /// The analog channels in the sample, the number specified by the analog_channel_count in the
    /// header
    pub analog_channels: Vec<i32>,
}

#[derive(Debug)]
pub enum RLDError {
    IO(io::Error),
    UTF8(FromUtf8Error),
    Conversion(TryFromIntError),
}

impl fmt::Display for RLDError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            RLDError::IO(e) => e.fmt(f),
            RLDError::UTF8(e) => e.fmt(f),
            RLDError::Conversion(e) => e.fmt(f),
        }
    }
}

impl error::Error for RLDError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self {
            RLDError::IO(ref e) => Some(e),
            RLDError::UTF8(ref e) => Some(e),
            RLDError::Conversion(ref e) => Some(e),
        }
    }
}

impl From<io::Error> for RLDError {
    fn from(err: io::Error) -> RLDError {
        RLDError::IO(err)
    }
}

impl From<FromUtf8Error> for RLDError {
    fn from(err: FromUtf8Error) -> RLDError {
        RLDError::UTF8(err)
    }
}

impl From<TryFromIntError> for RLDError {
    fn from(err: TryFromIntError) -> RLDError {
        RLDError::Conversion(err)
    }
}

impl RLDChannel {
    pub fn parse(readable: &mut impl Read) -> Result<Self, RLDError> {
        let unit = match readable.read_i32::<LittleEndian>()? {
            0 => RLDUnit::UnitLess,
            1 => RLDUnit::Voltage,
            2 => RLDUnit::Current,
            3 => RLDUnit::Binary,
            4 => RLDUnit::DataValid,
            5 => RLDUnit::Illuminance,
            6 => RLDUnit::Temperature,
            7 => RLDUnit::Integer,
            8 => RLDUnit::Percent,
            9 => RLDUnit::Pressure,
            _ => RLDUnit::Undefined,
        };

        let scale = readable.read_i32::<LittleEndian>()?;
        let data_size = readable.read_u16::<LittleEndian>()?;
        let valid_data_link = readable.read_u16::<LittleEndian>()?;
        let mut name = [0u8; 16];
        readable.read_exact(&mut name)?;
        Ok(RLDChannel {
            unit,
            scale,
            data_size,
            valid_data_link,
            name: String::from_utf8(name.to_vec())?,
        })
    }
}

impl RLDHeader {
    pub fn parse(readable: &mut impl Read) -> Result<Self, RLDError> {
        let mut magic = [0u8; 4];
        readable.read_exact(&mut magic)?;
        let version = readable.read_u16::<LittleEndian>()?;
        let length = readable.read_u16::<LittleEndian>()?;
        let block_size = readable.read_u32::<LittleEndian>()?;
        let block_count = readable.read_u32::<LittleEndian>()?;
        let sample_count = readable.read_u64::<LittleEndian>()?;
        let sampling_rate = readable.read_u16::<LittleEndian>()?;
        let mut mac_address = [0u8; 6];
        readable.read_exact(&mut mac_address)?;
        let start_time_s = readable.read_i64::<LittleEndian>()?;
        let start_time_ns = readable.read_i64::<LittleEndian>()?;
        let comment_length = readable.read_u32::<LittleEndian>()?;
        let binary_channel_count = readable.read_u16::<LittleEndian>()?;
        let analog_channel_count = readable.read_u16::<LittleEndian>()?;
        let mut comment = vec![0u8; usize::try_from(comment_length)?];
        readable.read_exact(&mut comment)?;
        let comment = String::from_utf8(comment)?;

        let mut channels = Vec::<RLDChannel>::new();
        for _ in 0..(binary_channel_count + analog_channel_count) {
            let channel = RLDChannel::parse(readable)?;
            channels.push(channel);
        }

        let header = RLDHeader {
            magic,
            version,
            length,
            block_size,
            block_count,
            sample_count,
            sampling_rate,
            mac_address,
            start_time_s,
            start_time_ns,
            comment_length,
            binary_channel_count,
            analog_channel_count,
            comment,
            channels,
        };

        Ok(header)
    }
}

impl RLDBlock {
    pub fn parse(header: &RLDHeader, readable: &mut impl Read) -> Result<Self, RLDError> {
        let real_s = readable.read_i64::<LittleEndian>()?;
        let real_ns = readable.read_i64::<LittleEndian>()?;
        let mono_s = readable.read_i64::<LittleEndian>()?;
        let mono_ns = readable.read_i64::<LittleEndian>()?;

        let realtime = NaiveDateTime::from_timestamp(real_s, real_ns.try_into()?);
        let monotime = NaiveDateTime::from_timestamp(mono_s, mono_ns.try_into()?);
        println!("real: {} mono: {}", realtime, monotime);

        let samples_per_block = header.block_size;
        let mut samples = Vec::<RLDSample>::new();
        for _ in 0..samples_per_block {
            samples.push(RLDSample::parse(header, readable)?);
        }

        Ok(RLDBlock {
            realtime_ts: realtime,
            monotonic_ts: monotime,
            samples,
        })
    }
}

impl RLDSample {
    pub fn parse(header: &RLDHeader, readable: &mut impl Read) -> Result<Self, RLDError> {
        let mut binary_values = Vec::<u32>::new();
        let binary_channel_count = header.binary_channel_count;
        let binary_count = (binary_channel_count + 31) / 32;
        for _ in 0..binary_count {
            binary_values.push(readable.read_u32::<LittleEndian>()?);
        }

        let analog_channel_count = header.analog_channel_count;
        let mut analog_channels = Vec::<i32>::new();
        for i in 0..analog_channel_count {
            let channels_index: usize = (binary_channel_count + i).into();
            // FIXME this is not great, because the standard allows for other data_sizes, but we
            // don't support that right now
            assert!(header.channels[channels_index].data_size == 4);
            analog_channels.push(readable.read_i32::<LittleEndian>()?);
        }

        Ok(RLDSample {
            binary_values,
            analog_channels,
        })
    }
}

impl<'a> RLDFile<'a> {
    pub fn parse(readable: &'a mut impl Read) -> Result<Self, RLDError> {
        let header = RLDHeader::parse(readable)?;

        Ok(RLDFile {
            header,
            file: readable,
        })
    }

    pub fn next_block(&mut self) -> Result<RLDBlock, RLDError> {
        RLDBlock::parse(&self.header, &mut self.file)
    }
}
